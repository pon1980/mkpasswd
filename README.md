## Building
To build `mkpasswd` follow these instructions

```
mkdir -p GTROOT
cd GTROOT
git clone https://bitbucket.org/pon1980/gtlib-ocaml.git
git clone https://bitbucket.org/pon1980/mkpasswd.git
make -C gtlib-ocaml byte
make -C mkpasswd byte
```

when completed the binary can be found in `GTROOT/mkpasswd/_build/dist`.

## Usage

```
> mkpasswd --help
Usage: mkpasswd [-c <count>] [-s <seed>] --4x5 | --4x4 | <pattern>
  --seed  (-s) a integer to seed the PRNG (default: none)
  --count  (-c) number of passwords to generate (default: 1)
  --4x5 generate a password on the form: GLKN-JRVV-J3FJ-08BK-BRHA
  --4x4 generate a password on the form: yww1 wmow uzn1 9xlk
  -help  Display this list of options
  --help  Display this list of options
```
