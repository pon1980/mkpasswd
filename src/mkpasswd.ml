(*
 * Copyright 2019 Gacrux Technology AB
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)

open GTLib.IO.Print
module R = GTLib.Math.Rnd_string

let g_Seed : int option ref = ref None
let g_Pattern : R.Parser.t option ref = ref None
let g_Count = ref 1

exception Argument_exp of string

let spec =
  [ ( "--seed",
       Arg.Int (fun seed -> g_Seed := Some seed),
       " (-s) a integer to seed the PRNG (default: none)");
     ( "-s",
       Arg.Int (fun seed -> g_Seed := Some seed),
       "");
     ( "--count",
       Arg.Int (fun count -> g_Count := count),
       " (-c) number of passwords to generate (default: 1)"); 
     ( "-c",
       Arg.Int (fun count -> g_Count := count),
       "");
     ( "--4x5",
       Arg.Unit 
        (fun () -> g_Pattern := Some (R.make "([0-9A-Z]{4}-){4}[0-9A-Z]{4}")),
       "generate a password on the form: GLKN-JRVV-J3FJ-08BK-BRHA");
     ( "--4x4",
       Arg.Unit
        (fun () -> g_Pattern := Some (R.make "([0-9a-z]{4} ){3}[0-9a-z]{4}")),
        "generate a password on the form: yww1 wmow uzn1 9xlk");
     ( "--",
       Arg.Rest (fun pattern -> g_Pattern := Some (R.make pattern)),
       "") ]

let option_extract = function
  | Some value -> value
  | None -> raise (Argument_exp "Expected argument!")

let check_Arguments () =
  begin
    (match !g_Seed with
     | None -> Random.self_init ()
     | Some seed -> Random.init seed);

    (match !g_Pattern with
     | None -> raise (Argument_exp "Pattern expected!")
     | Some _ -> ());

    if !g_Count < 0
      then raise (Argument_exp "Argument 'count' must be positive!")
      else ();
  end

let main () =
  try
    let () =
      Arg.parse
        spec 
        (fun opt -> g_Pattern := Some (R.make opt))
        "Usage: mkpasswd [-c <count>] [-s <seed>] --4x5 | --4x4 | <pattern>"
    in
    let () = check_Arguments () in
    let rec loop n =
      if n = 0
        then ()
        else
          begin
            print str (R.sample (option_extract !g_Pattern)) lf to_stdout;
            loop (n - 1)
          end
    in
    begin
      loop !g_Count;
      exit 0
    end
  with
  | Argument_exp error -> 
      (print str "Bad argument: " str error lf to_stderr; exit 1)
  | _ ->
      (print str "Unknown error!" lf to_stderr; exit 2)


let _ = main ()
